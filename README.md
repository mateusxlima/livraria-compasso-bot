# Bot bibliotecário

Chat bot criado com bot framework em Node.js, para dar assistência à compras de livros online.

O Bot bibliotecário é uma POC, na empresa Compasso.

#### Para executar:

- OBS: Antes de excutar você precisa adicionar suas próprias credenciais do LUIS, e também adicionar a url de uma API de venda de livros.

`npm intall`

`npm start`
